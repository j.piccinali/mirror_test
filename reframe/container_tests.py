# {{{ MIT License
#
# Copyright (c) 2021 CSCS, ETH Zurich
#               2021 University of Basel
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# @author: jgphpc }}}
import os

import reframe as rfm
import reframe.utility.sanity as sn
import reframe.utility.udeps as udeps


# ----------------------------------------------------------------------------
# Run with: https://git.scicore.unibas.ch/j.piccinali/mirror_test.git/.gitlab/
# ----------------------------------------------------------------------------
containers_list = ['Singularity']
images_list = [
    # 'debian11_FAIL.def.sif',
    'debian11_clang11_mpich341.def.sif',
    'debian11_clang12_mpich341.def.sif',
    'debian11_clang13_mpich341.def.sif',
    'debian11_gnu09_mpich341.def.sif',
    'debian11_gnu10_mpich341.def.sif',
    'debian11_nvhpc219_cuda114_mpich341.def.sif',
]
image_path = '/storage/shared/projects/sph-exa/sph-exa2'
source_topdir = r'${SPHEXA_TOPDIR}'


# {{{ BuildAppInsideContainer:
@rfm.simple_test
class BuildAppInsideContainer(rfm.RunOnlyRegressionTest):
    platform = parameter(containers_list)
    image = parameter(images_list)

    valid_systems = ['dmi:login']
    valid_prog_environs = ['builtin']

    def make_script(self, patchfile, source_topdir, executable):
        # NOTE: f-string expression can not include '#' or a backslash
        cuda_vars = ''
        cuda_flags = ''
        if executable == 'sedov-cuda':
            cuda_vars = """
                echo
                echo NVHPC=$NVHPC
                echo NVIDIA_VERSION=$NVIDIA_VERSION
                echo CUDATOOLKIT_VERSION=$CUDATOOLKIT_VERSION
                echo NVIDIA_PATH=$NVIDIA_PATH
                echo -n PATH= ;echo $PATH |tr : "\\n"
                export MPICH_CXX=`which nvc++` && echo MPICH_CXX=$MPICH_CXX
                echo
            """
            cuda_flags = (
                r'-DCMAKE_CXX_FLAGS=-I${NVIDIA_PATH}/cuda/'
                r'${CUDATOOLKIT_VERSION}/targets/x86_64-linux/include'
                # r' MPICH_CXX=nvc++'
            )

        sed_str = f"""
        cd /rfm_workdir
        mkdir -p builddir installdir && cd builddir
        {cuda_vars}
        echo source_topdir={source_topdir}
        cmake -DCMAKE_CXX_COMPILER=mpicxx \\
            {cuda_flags} \\
            -DBUILD_TESTING=OFF \\
            {source_topdir}
        make help && make -j4 {executable} VERBOSE=1 && \\
        cp src/sedov/sedov* ../installdir
        """
        try:
            f = open(patchfile, "w")
            f.write(sed_str)
            f.close()
            return patchfile
        except Exception as e:
            sys.stderr.write(f'make_script failed!: {patchfile} {e}')
            return None

    @run_before('run')
    def setup_container(self):
        self.descr = f'Build app inside a container using {self.platform}'
        # image_prefix = 'docker://' if self.platform == 'Singularity' else ''
        self.container_platform = self.platform
        self.container_platform.image = os.path.join(image_path, self.image)
        container_path = '/usr/local/games/sph'
        self.container_platform.mount_points = [
            # /rfm_workdir
            (source_topdir, f'{container_path}/'),
            # ('${SPHEXA_TOPDIR}', f'{container_path}/'),
            # ('/path/to/host/dir1', '/path/to/container/mount_point1'),
            # -S <path-to-source> = Explicitly specify a source directory.
            # -B <path-to-build>  = Explicitly specify a build directory.
        ]
        self.prerun_cmds = [
            f'echo SPHEXA_TOPDIR={source_topdir}',
            f'echo > {source_topdir}/domain/test/CMakeLists.txt',
        ]
        if 'cuda' in self.image:
            executable = 'sedov-cuda'
        else:
            executable = 'sedov'

# {{{
#         cmd_str = (
#             r'cd /rfm_workdir',
#             r'mkdir -p builddir installdir && cd builddir',
#             fr'echo source_topdir={source_topdir}',
#             # r'echo $PATH |tr : "\n"',
#             # r'echo MPICH_CXX=$MPICH_CXX && '
#             # r'echo MPICH_CC=$MPICH_CC && '
#             r'cmake -DCMAKE_CXX_COMPILER=mpicxx '
#             rf'-DCMAKE_CXX_FLAGS=-I{cuda_inc_path} '
#             rf'-DBUILD_TESTING=OFF '
#             rf'{source_topdir}',
#             rf'make help && make -j4 {executable} VERBOSE=1 &&',
#             r'cp src/sedov/sedov* ../installdir'
#         )
# }}}
# {{{
#         cmd_str = (
#             r'cd /rfm_workdir && mkdir -p builddir installdir && '
#             r'cd builddir && '
#             fr'echo top={source_topdir} && '
#             # r'which nvc++ && export MPICH_CXX=nvc++ && '
#             # r'echo NVHPC=$NVHPC && '
#             # r'echo NVIDIA_VERSION=$NVIDIA_VERSION && '
#             # r'echo CUDATOOLKIT_VERSION=$CUDATOOLKIT_VERSION && '
#             # r'echo NVIDIA_PATH=$NVIDIA_PATH && '
#             r'echo $PATH |tr : "\n" && '
#             r'echo MPICH_CXX=$MPICH_CXX && '
#             r'echo MPICH_CC=$MPICH_CC && '
#             r'cmake -DCMAKE_CXX_COMPILER=mpicxx '
#             rf'-DCMAKE_CXX_FLAGS=-I{cuda_inc_path} '
#             rf'-DBUILD_TESTING=OFF '
#             rf'{source_topdir} && '
#             # rf'-D CMAKE_INSTALL_PREFIX=/rfm_workdir/installdir '
#             # r'${SPHEXA_TOPDIR} && '
#             rf'make help && make -j4 {executable} VERBOSE=1 && '
#             r'cp src/sedov/sedov* ../installdir'
#             # rf'cd /rfm_workdir;pwd;ls -la;'
#         )
# }}}
        patchfile = os.path.join(self.stagedir, '0.sh')
        res = self.make_script(patchfile, source_topdir, executable)
        self.container_platform.command = (
            f"bash '{patchfile}'"
            # f"bash -c '{cmd_str}'"
            # "bash -c 'cat /etc/os-release | tee /rfm_workdir/release.txt'"
        )

    @sanity_function
    def sanity_check(self):
        # -- Found MPI: TRUE (found version "3.1")
        # -- Found OpenMP: TRUE (found version "4.5")
        return sn.assert_found(r'Found MPI: TRUE|FIXME', self.stdout)
# }}}


# {{{ RunAppInsideContainer:
@rfm.simple_test
class RunAppInsideContainer(rfm.RunOnlyRegressionTest):
    platform = parameter(containers_list)
    image = parameter(images_list)

    sourcesdir = None
    valid_systems = ['dmi:a100']
    valid_prog_environs = ['builtin']
    # num_tasks = 1
    # num_tasks = 12

    @run_after('init')
    def set_dependencies(self):
        # NOTE: deps will be ignored if --skip-system-check
        dep_name = (
            f'BuildAppInsideContainer_{self.platform}_'
            f'{self.image.replace(".def.sif", "_def_sif")}'
        )
        self.depends_on(dep_name, how=udeps.fully)

    @require_deps
    def set_executable(self):
        self.descr = f'Run app inside a container using {self.platform}'
        self.container_platform = self.platform
        self.container_platform.image = os.path.join(image_path, self.image)
        dep_name = (
            f'BuildAppInsideContainer_{self.platform}_'
            f'{self.image.replace(".", "_")}'
        )
        stagedir = self.getdep(
            dep_name, environ='builtin', part='login').stagedir
        if 'cuda' in self.image:
            self.num_tasks = 1
            myexe = os.path.join(stagedir, 'installdir', 'sedov-cuda')
            self.container_platform.with_cuda = True
        else:
            self.num_tasks = 12
            myexe = os.path.join(stagedir, 'installdir', 'sedov')

        self.container_platform.command = f'bash -c "{myexe} -s 1 -n 50"'

    @sanity_function
    def sanity_check(self):
        regex = r'Total execution time of \d+ iterations|FIXME'
        return sn.assert_found(regex, self.stdout)
# }}}
