# Copyright 2016-2021 Swiss National Supercomputing Centre (CSCS/ETH Zurich)
# ReFrame Project Developers. See the top-level LICENSE file for details.
#
# SPDX-License-Identifier: BSD-3-Clause
#
# ReFrame CSCS settings
#

import reframe.utility.osext as osext


site_configuration = {
    'systems': [
        {
            'name': 'dmi',
            'descr': 'DMI unibas.ch',
            'hostnames': [
                'dmi'
            ],
            'modules_system': 'lmod',
            'resourcesdir': '/users/staff/dmi-dmi/piccinal/resources',
            'partitions': [
                {
                    'name': 'login',
                    'scheduler': 'local',
                    'container_platforms': [
                        {
                            'type': 'Sarus',
                            'modules': [
                                'sarus'
                            ]
                        },
                        {
                            'type': 'Singularity',
                            # 'modules': [
                            #     'singularity/3.6.4-daint'
                            # ]
                        }
                    ],
                    'modules': [],
                    'access': [],
                    'environs': [
                        'builtin',
                        'gnu+openmpi',
                        # 'PrgEnv-gnu',
                        # 'PrgEnv-cray',
                        # 'PrgEnv-intel',
                        # 'PrgEnv-pgi'
                    ],
                    'descr': 'Login nodes',
                    'max_jobs': 40,
                    'launcher': 'local'
                },
                {
                    'name': 'a100',
                    'scheduler': 'slurm',
                    'container_platforms': [
                        {
                            'type': 'Sarus',
                            'modules': [
                                'sarus'
                            ]
                        },
                        {
                            'type': 'Singularity',
                            # 'modules': [
                            #     'singularity/3.6.4-daint'
                            # ]
                        }
                    ],
                    'time_limit': '10m',
                    # 'modules': [
                    #     'daint-gpu'
                    # ],
                    'access': ['--partition=gpu', '--exclusive'],
                    # ignored: '--gres=gpu:1'
                    # 'access': [
                    #     f'--partition=gpu',
                    #     f'--constraint=gpu',
                    #     f'--account={osext.osgroup()}'
                    # ],
                    'environs': [
                        'builtin',
                        'gnu+openmpi',
                        # 'PrgEnv-gnu',
                        # 'PrgEnv-cray',
                        # 'PrgEnv-intel',
                        # 'PrgEnv-pgi'
                    ],
                    'descr': 'A100 gpu',
                    'max_jobs': 100,
                    'launcher': 'srun',
                    'resources': [
                        {
                            'name': 'gres',
                            'options': ['--gres={gres}']
                        }
                    ]
                },
                {
                    'name': 'xeon',
                    'scheduler': 'slurm',
                    'container_platforms': [
                        {
                            'type': 'Sarus',
                            'modules': [
                                'sarus'
                            ]
                        },
                        {
                            'type': 'Singularity',
                            # 'modules': [
                            #     'singularity/3.6.4-daint'
                            # ]
                        }
                    ],
                    'time_limit': '10m',
                    'access': ['--partition=xeon', '--exclusive'],
                    'environs': [
                        'builtin',
                        'gnu+openmpi',
                        # 'PrgEnv-gnu',
                        # 'PrgEnv-cray',
                        # 'PrgEnv-intel',
                        # 'PrgEnv-pgi'
                    ],
                    'descr': 'Intel(R) Xeon(R) CPU E5-2640 v4 @ 2.40GHz',
                    'max_jobs': 100,
                    'launcher': 'srun',
#                     'resources': [
#                         {
#                             'name': 'gres',
#                             'options': ['--gres={gres}']
#                         }
#                     ]
                },
            ]
        },
        {
            'name': 'generic',
            'descr': 'Generic fallback system',
            'partitions': [
                {
                    'name': 'default',
                    'scheduler': 'local',
                    'environs': [
                        'builtin'
                    ],
                    'descr': 'Login nodes',
                    'launcher': 'local'
                }
            ],
            'hostnames': ['.*']
        }
    ],
    'environments': [
        {
            'name': 'PrgEnv-cray',
            'modules': [
                'PrgEnv-cray'
            ]
        },
        {
            'name': 'gnu+openmpi',
            'modules': [
                'nvhpc/2021_219-cuda-11.4',
                'OpenMPI/4.0.5-GCC-10.2.0',
                'libfabric/1.11.0-GCCcore-10.2.0',
            ],
            'cc':  'mpicc',
            'cxx': 'mpicxx',
            'ftn': 'mpif90',
        },
        {
            'name': 'PrgEnv-gnu',
            'modules': [
                # 'GCCcore/10.2.0',  # loaded by Python/3.8.6-GCCcore-10.2.0
                'nvhpc/2021_213-cuda-11.2',
                'OpenMPI/4.0.5-GCC-10.2.0',
                'libfabric/1.11.0-GCCcore-10.2.0',
                # 'PrgEnv-gnu'
            ],
            'cc':  'mpicc',
            'cxx': 'mpicxx',
            'ftn': 'mpif90',
        },
        {
            'name': 'PrgEnv-intel',
            'modules': [
                'PrgEnv-intel'
            ]
        },
        {
            'name': 'PrgEnv-pgi',
            'modules': [
                'PrgEnv-pgi'
            ]
        },
        {
            'name': 'builtin',
            'cc': 'cc',
            'cxx': 'CC',
            'ftn': 'ftn'
        },
        {
            'name': 'builtin-gcc',
            'cc': 'gcc',
            'cxx': 'g++',
            'ftn': 'gfortran'
        }
    ],
    'logging': [
        {
            'handlers': [
                {
                    'type': 'file',
                    'name': 'reframe.log',
                    'level': 'debug2',
                    'format': '[%(asctime)s] %(levelname)s: %(check_info)s: %(message)s',   # noqa: E501
                    'append': False
                },
                {
                    'type': 'stream',
                    'name': 'stdout',
                    'level': 'info',
                    'format': '%(message)s'
                },
                {
                    'type': 'file',
                    'name': 'reframe.out',
                    'level': 'info',
                    'format': '%(message)s',
                    'append': False
                }
            ],
            'handlers_perflog': [
                {
                    'type': 'filelog',
                    'prefix': '%(check_system)s/%(check_partition)s',
                    'level': 'info',
                    'format': '%(check_job_completion_time)s|reframe %(version)s|%(check_info)s|jobid=%(check_jobid)s|num_tasks=%(check_num_tasks)s|%(check_perf_var)s=%(check_perf_value)s|ref=%(check_perf_ref)s (l=%(check_perf_lower_thres)s, u=%(check_perf_upper_thres)s)|%(check_perf_unit)s',   # noqa: E501
                    'datefmt': '%FT%T%:z',
                    'append': True
                },
#                 {
#                     'type': 'httpjson',
#                     'url': 'http://httpjson-server:12345/rfm',
#                     'level': 'info',
#                     'extras': {
#                         'facility': 'reframe',
#                         'data-version': '1.0',
#                     }
#                 }
            ]
        }
    ],
    'modes': [
#         {
#             'name': 'maintenance',
#             'options': [
#                 '--unload-module=reframe',
#                 '--exec-policy=async',
#                 '--strict',
#                 '--output=$APPS/UES/$USER/regression/maintenance',
#                 '--perflogdir=$APPS/UES/$USER/regression/maintenance/logs',
#                 '--stage=$SCRATCH/regression/maintenance/stage',
#                 '--report-file=$APPS/UES/$USER/regression/maintenance/reports/maint_report_{sessionid}.json',
#                 '-Jreservation=maintenance',
#                 '--save-log-files',
#                 '--tag=maintenance',
#                 '--timestamp=%F_%H-%M-%S'
#             ]
#         },
#         {
#             'name': 'production',
#             'options': [
#                 '--unload-module=reframe',
#                 '--exec-policy=async',
#                 '--strict',
#                 '--output=$APPS/UES/$USER/regression/production',
#                 '--perflogdir=$APPS/UES/$USER/regression/production/logs',
#                 '--stage=$SCRATCH/regression/production/stage',
#                 '--report-file=$APPS/UES/$USER/regression/production/reports/prod_report_{sessionid}.json',
#                 '--save-log-files',
#                 '--tag=production',
#                 '--timestamp=%F_%H-%M-%S'
#             ]
#         }
    ],
    'general': [
        {
            'check_search_path': [
                'checks/'
            ],
            'check_search_recursive': True
        }
    ]
}
