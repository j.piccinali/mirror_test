# {{{ MIT License
#
# Copyright (c) 2021 CSCS, ETH Zurich
#               2021 University of Basel
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# @author: jgphpc }}}
import os

import reframe as rfm
import reframe.utility.sanity as sn
import reframe.utility.udeps as udeps


# ----------------------------------------------------------------------------
# Run with: https://git.scicore.unibas.ch/j.piccinali/mirror_test.git/.gitlab/
# ----------------------------------------------------------------------------
# {{{ container images names: clang13,12,11/gnu10,9/nvhpc219+cuda114
# source_readonlydir = r'${SPHEXA_TOPDIR}'
# os.environ["SPHEXA_TOPDIR"] = os.path.join(image_path, source_readonlydir)
image_path_storage = '/storage/shared/projects/sph-exa/sph-exa2'
image_path = r'${SPHEXA_TOPDIR}'
source_dir = 'SPH-EXA_mini-app.git'
source_readonlydir = image_path  # os.path.join(image_path, source_dir)
cmake_txt_in = os.path.join(source_dir, 'domain', 'test', 'CMakeLists.txt.in')
googletest_dir = 'googletest.git'
googletest_readonlydir = os.path.join(image_path_storage, googletest_dir)
in_file = os.path.join(image_path_storage, source_dir, 'bigfiles',
                       'Test3DEvrardRel.bin')
# }}}
# {{{ test names:
serial_unittests = {
    'component_units': 'builddir/domain/test/unit',
    'component_units_omp': 'builddir/domain/test/unit',
    'kernel_tests': 'builddir/include/sph/test/kernel',
    'coordinate_test': 'builddir/domain/test/coord_samples',
}
path_mpi_unittests = 'builddir/domain/test/integration_mpi'
mpi_unittests_2ranks = {
    'domain_2ranks': path_mpi_unittests,
    'exchange_focus': path_mpi_unittests,
    'exchange_halos': path_mpi_unittests,
    'globaloctree': path_mpi_unittests,
}
mpi_unittests = {
    'treedomain': path_mpi_unittests,
    'focus_tree': path_mpi_unittests,
    'box_mpi': path_mpi_unittests,
    'exchange_keys': path_mpi_unittests,
    'exchange_domain': path_mpi_unittests,
    'domain_nranks': path_mpi_unittests,
}
path_perf_unittests = 'builddir/domain/test/performance'
perf_unittests = {
    'hilbert_perf': path_perf_unittests,
    'peers_perf': path_perf_unittests,
    'octree_perf': path_perf_unittests,
    'scan_perf': path_perf_unittests,
}
path_cpu_tests = 'builddir/src'
cpu_tests = {
    'sedov': f'{path_cpu_tests}/sedov',
    'evrard_new': f'{path_cpu_tests}/evrard',
}
gpu_tests = {
    'component_units_cuda': 'builddir/domain/test/unit_cuda',
    'sedov-cuda': f'{path_cpu_tests}/sedov',
    # 'evrard-cuda': f'{path_cpu_tests}/evrard',
}
all_tests = {
    **mpi_unittests_2ranks,
    **mpi_unittests,
    **perf_unittests,
    **cpu_tests,
    **serial_unittests,
    **gpu_tests}
all_tests_name = list(all_tests.keys())

# {{{ serial_unittests = [
#     # 'collision_reference/collisions_a2a_test',
#     # 'domain/test/unit/component_units',
#     # 'unit/component_units_omp'
# builddir/domain/test/coord_samples/coordinate_test
# builddir/domain/test/unit/component_units
# builddir/domain/test/unit/component_units_omp
# ] }}}

# cpu_archs = ['cascadelake']
cpu_archs = ['broadwell', 'cascadelake']
# cascadelake/dmi:gpu = Intel(R) Xeon(R) Gold 6258R CPU @ 2.70GHz 
#                       + NVIDIA A100-PCIE-40GB
# broadwell/dmi:xeon = Intel(R) Xeon(R) CPU E5-2640 v4 @ 2.40GHz
# }}}


# {{{ Build
@rfm.simple_test
class Build(rfm.CompileOnlyRegressionTest):
    '''Build the unittests and tests before running them'''
    # target_cpu_arch = parameter(['broadwell', 'cascadelake'])
    target_cpu_arch = parameter(cpu_archs)

    valid_systems = ['dmi:login']
    valid_prog_environs = ['gnu+openmpi']
    build_system = 'CMake'
    # {{{ fix for cmakelist:
    prebuild_cmds = [
        # cmds running before cd builddir:
        'module list', 'which mpicxx', 'mpicxx --version',
        f'echo SPHEXA_TOPDIR={source_readonlydir}',
        f'cp -a {source_readonlydir} .',
        # f'cp -a {googletest_readonlydir} .',
        # ---
        '# {{{ sed',
        f'# --- setup path to googletest.git:',
        f'sed -i \'s@GIT_REPOSITORY@URL "{googletest_readonlydir}"\\n#@\' '
        f'{cmake_txt_in}',
        # ---
        f'sed -i \'s@GIT_TAG@#@\' {cmake_txt_in}',
        f'# --- {cmake_txt_in}:',
        f'cat {cmake_txt_in}',
        f'# --- turn off googletest with: echo > {cmake_txt_in}',
        # f'# --- remove evrard test until fixed:',
        # f'sed -i \'s@(add_subdirectory(evrard))@# @\' '
        # f'{source_dir}/src/CMakeLists.txt',
        f'# --- remove -march=native:',
        f'sed -i \'s@set(CMAKE_CXX_FLAGS_RELEASE@# @\' '
        f'{source_dir}/CMakeLists.txt',
        '# }}}',
    ]
    # postbuild_cmds = ['make help']
    # }}}
    maintainers = ['JG']
    tags = {'gitlab'}

    @run_before('compile')
    def set_cmake_options(self):
        self.build_system.builddir = 'builddir'
        # --- g++ --help=target |grep "valid arguments for -march= option"
        target_cpu_arch = f'-O2 -march={self.target_cpu_arch} -DNDEBUG'
        self.build_system.config_opts = [
            # '-DCMAKE_CXX_COMPILER=mpicxx',
            # '-DCMAKE_VERBOSE_MAKEFILE=TRUE',
            '-DCMAKE_BUILD_TYPE=Release',
            '-DBUILD_TESTING=ON',
            '-DCMAKE_EXE_LINKER_FLAGS=-lpthread',
            f'-DCMAKE_CXX_FLAGS_RELEASE="{target_cpu_arch}"',
            f'-S ../{source_dir}', '2>&1 #',
        ]
        self.build_system.max_concurrency = 21
        # TODO: self.current_partition.processor.num_cpus_per_socket
        self.build_system.make_opts = [f'help {" ".join(all_tests_name)}']
        # self.build_system.make_opts = ['help all']  # ['help sedov']

    # {{{ sanity
    @sanity_function
    def sanity_check(self):
        # -- The CXX compiler identification is GNU 9.3.0
        # -- Found MPI: TRUE (found version "3.1")
        # -- Found OpenMP: TRUE (found version "4.5")
        # -- The CUDA compiler identification is NVIDIA 11.4.100
        regex1 = r'The CXX compiler identification is'
        regex2 = r'Found MPI: TRUE'
        regex3 = r'Found OpenMP: TRUE'
        regex4 = r'The CUDA compiler identification is'
        return sn.all([
            sn.assert_found(regex1, self.stdout),
            sn.assert_found(regex2, self.stdout),
            sn.assert_found(regex3, self.stdout),
            sn.assert_found(regex4, self.stdout),
        ])
    # }}}
# }}}


# {{{ Run
@rfm.simple_test
class Run(rfm.RunOnlyRegressionTest):
    target_cpu_arch = parameter(cpu_archs)
    test_executable = parameter(all_tests_name)

    valid_prog_environs = ['gnu+openmpi']
    postrun_cmds = ['echo rc=$?']

    @run_after('init')
    def set_valid_systems(self):
        if self.target_cpu_arch == 'broadwell':
            self.valid_systems = ['dmi:xeon']
        elif self.target_cpu_arch == 'cascadelake':
            self.valid_systems = ['dmi:a100']
        else:
            print(f'unknown self.target_cpu_arch={self.target_cpu_arch}...')

    @run_after('init')
    def set_dependencies(self):
        # NOTE: deps will be ignored if --skip-system-check
        dep_name = f'Build_{self.target_cpu_arch}'
        self.depends_on(dep_name, how=udeps.fully)

    @require_deps
    def set_executable(self):
        self.descr = f'Running test'
        dep_name = f'Build_{self.target_cpu_arch}'
        stagedir = self.getdep(
            dep_name, environ='gnu+openmpi', part='login').stagedir
        myexe_args = []
        self.executable = os.path.join(
            stagedir, all_tests[self.test_executable], self.test_executable)
        # {{{ corner cases:
        if self.test_executable in perf_unittests:
            self.num_tasks = 1  # these tests are OpenMP threaded
            # self.num_cpus_per_task = 10

        if self.test_executable in mpi_unittests_2ranks:
            self.num_tasks = 2

        if self.test_executable in mpi_unittests:
            self.num_tasks = 10
            # TODO: self.num_tasks = \
            # self.current_partition.processor.num_cpus_per_socket

        if 'cuda' in self.test_executable:
            self.num_tasks = 1
            # self.container_platform.with_cuda = True

        if 'evrard_new' in self.test_executable:
            myexe_args = ['-s', '1', '--input', in_file]
            # nLocalParticles 16698

        if 'sedov' in self.test_executable:
            myexe_args = ['-s', '1', '-n', '50']

        self.executable_opts = myexe_args
        # --- skip executables built with cuda & non gpu partition:
        assert_1 = 'cuda' in self.test_executable
        assert_2 = self.current_partition.name == 'xeon'
        self.skip_if(
            assert_1 and assert_2,
            f'exe={self.test_executable} & part={self.current_partition.name}')
        # }}}

    # {{{ sanity
    @sanity_function
    def sanity_check(self):
        regex_unittest = r'PASSED.*\d+ test'
        regex_scitest = r'Total execution time of \d+ iterations'
        regex_tests = {
            # 'box_mpi': regex_unittest, # 3
            # 'coordinate_test': regex_unittest, # 1
            # 'domain_2ranks': regex_unittest,  # 6
            # 'domain_nranks': regex_unittest,  # 3
            # 'exchange_domain': regex_unittest,  # 2
            # 'exchange_focus': regex_unittest,  # 3
            # 'exchange_halos': regex_unittest,  # 1
            # 'exchange_keys': regex_unittest, # 1
            # 'focus_tree': regex_unittest, # 1
            # 'globaloctree': regex_unittest, # 1
            # 'treedomain': regex_unittest, # 1
            # 'kernel_tests': regex_unittest,  # 6
            #
            'sedov': regex_scitest,
            'sedov-cuda': regex_scitest,
            'evrard_new': regex_scitest,
            'evrard-cuda': regex_scitest,
            #
            'hilbert_perf': r'hilbert keys: \S+ s on CPU',
            'octree_perf': r'octree halo discovery: \S+',
            'peers_perf': r'find peers: \S+',
            'scan_perf': r'parallel benchmark bandwidth: \S+ MB/s',
        }
        exe = self.test_executable  # .split("/")[-1]
        all_exes = list( mpi_unittests_2ranks.keys() |
                         mpi_unittests.keys() |
                         serial_unittests.keys() )
        if exe in all_exes:
            regex = r'^rc=(\d+)'
            rc = sn.extractsingle(regex, self.stdout, 1)
            failure_msg = f'{exe} ctest failed with rc={rc}'
            return sn.assert_eq(rc, '0', failure_msg)
        else:
            return sn.assert_found(regex_tests[exe], self.stdout)
    # }}}

    # {{{ TODO: perf
    # The total neighbor sum and the different energies are a useful indicator
    # whether any change affected the numerics or not
    # ------------------------------------------
    # Check ### Total Neighbors: 6319573, Avg neighbor count per particle: 96
    # Check ### Total time: 0.000231, current time-step: 0.000121
    # Check ### Total energy: -0.607752, (internal: 0.0499007, 
    #                         cinetic: 3.97811e-08, gravitational: -0.657652)
    # ------------------------------------------
    # }}}
# }}}
