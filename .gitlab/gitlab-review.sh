#!/bin/bash

echo '---- ./.gitlab/gitlab-review.sh ----'
# must be inside git repo for gh to work
# cd SPH-EXA_mini-app.git
cd $SPHEXA_TOPDIR
pwd
# ls -la
# which gh

# --- get gh pr:
# inpr=$CI_PROJECT_DIR/gh_pr.txt  # see gitlab-ci.sh
inpr=$SPHEXA_TOPDIR/../gh_pr.txt  # see gitlab-ci.sh
ghpr=`grep github_pr= $inpr |awk -F= '{print $2}'`
# ghsha=`tail -1 $inpr`

# --- get reframe stats (without color):
inrfm=../reframe/reframe.out
rfmstatus=`grep 'test case(s) from' $inrfm |sed -r "s/\x1b\[([0-9]{1,2}(;[0-9]{1,2})?)?m//g"`
url="https://git.scicore.unibas.ch/j.piccinali/mirror_test/-/pipelines/$CI_PIPELINE_ID"

# add failure stats:
(echo -e '\n```' ;grep -A100 'FAILURE STATISTICS' ../reframe/reframe.out; echo '```' ;) > fails.txt

if [ -n $ghpr ] ;then
    gh pr comment $ghpr \
    -b "Check gitlab tests results before merging: $rfmstatus $url `cat $inpr` `cat fails.txt`"
    # - if [ "$GITLAB_USER_LOGIN" == "j.piccinali" ]; then gh pr comment -b "tests results: $rfm-status"; else ;fi
    # - gh pr review -r -b "running tests on minihpc" # if not piccinal
else
    echo "ghpr=$ghpr not found"
    cat $in
fi
cd -
