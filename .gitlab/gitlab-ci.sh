#!/bin/bash

# hypothesis: git clone done
echo '---- ./.gitlab/gitlab-ci.sh ----'
pwd
# ls -la
# which gh

# loop over gh runids and find matching pipeline/pr
runids=`gh run list -L 30 |awk -F'\t' '{print $7}'`;
echo "loop: runids=$runids"

# TODO: function
logfile=.eff.log
prfile=$JGROOTD/gh_pr.txt
# CI_PROJECT_DIR
for myrunid in $runids ;do
    gh run view $myrunid --log > $logfile ;rc=$?
    if [ $rc == 0 ] ;then
    # --- find pipeline id:
    gitlabci_pipeline_id_from_githubactions=`grep -m1 "/pipelines/" $logfile |awk -F"/pipelines/" '{print $2}' |cut -d\" -f1`
    # {{{ debug:
    echo -n "myrunid=$myrunid"
    echo -n " gitlabci_pipeline_id_from_githubactions=$gitlabci_pipeline_id_from_githubactions"
    echo " gitlab_CI_PIPELINE_ID=$CI_PIPELINE_ID"; # }}}
    if [ "$gitlabci_pipeline_id_from_githubactions" == "$CI_PIPELINE_ID" ]; then
        # --- good: gh and gl pipeline ids are the same
        echo "yes: myrunid=$myrunid"
        echo "gitlabci_pipeline_id_from_githubactions=$gitlabci_pipeline_id_from_githubactions"
        echo "gitlab_CI_PIPELINE_ID=$CI_PIPELINE_ID"
        # --- let's find the pr number:
        # echo "--- pr "
        # grep 'JG_GITHUB_PR=' $logfile
        # echo "--- pr "
        github_pr=`grep 'JG_GITHUB_PR=' $logfile |grep ' PR: ' |awk -F' PR: ' '{print $2}'`
        echo "github_pr=$github_pr" > $prfile
        if [ "$github_pr" != "" ] ;then
        #     echo "ko: github_pr=$github_pr"
        #     exit -1
        # else
            echo "ok: github_pr=$github_pr"
            gh pr checkout $github_pr
            git branch
            git log --pretty=oneline -n 1 |cut -c1-7 >> $prfile
            cat $prfile
            exit 0
        fi
    fi
    else
        cat $logfile
    fi
done

# {{{ was trying to find pr in a complicated way, find pr with gh action instead
# gh run view 1295359217 --log |grep 'build id: ' |awk -F'build id: ' '{print $2}'
# runids=`gh run list -L 10 |awk -F'\t' '{print $7}'`;
# echo "runids=$runids"
# 
# for myrunid in $runids ;do
#     gh run view $myrunid --log > .eff.log
#     # gitlabci_pipeline_id_from_githubactions=`gh run view $myrunid --log |grep -m1 "/pipelines/" |awk -F"/pipelines/" '{print $2}' |cut -d\" -f1`
#     gitlabci_pipeline_id_from_githubactions=`grep -m1 "/pipelines/" .eff.log |awk -F"/pipelines/" '{print $2}' |cut -d\" -f1`
#     # echo "myrunid=$myrunid"
#     # echo "gitlabci_pipeline_id_from_githubactions=$gitlabci_pipeline_id_from_githubactions"
#     # echo "gitlab_CI_PIPELINE_ID=$CI_PIPELINE_ID";
#     if [ "$gitlabci_pipeline_id_from_githubactions" == "$CI_PIPELINE_ID" ]; then
#         # good: gh and gl are the same
#         echo "yes:"
#         echo "myrunid=$myrunid"
#         echo "gitlabci_pipeline_id_from_githubactions=$gitlabci_pipeline_id_from_githubactions"
#         echo "gitlab_CI_PIPELINE_ID=$CI_PIPELINE_ID"
#         # -------------------------------------------
#         github_sha=`grep ' JG_GITHUB_SHA=' .eff.log |awk -FJG_GITHUB_SHA= '{print $2}'`
#         git checkout $github_sha
#         git log --pretty=oneline -n 3
#         exit 0
#     else
#         "echo commit not found, skipping..."
#         exit -1
#     fi
# done
# }}}
